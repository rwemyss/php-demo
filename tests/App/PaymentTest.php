<?php
/**
 * Payment Test
 *
 * @author     Richard Wemyss <rwemyss@gmail.com>
 * @copyright   Richard Wemyss
 */

namespace AppTest;

use PHPUnit\Framework\TestCase;

class PaymentTest extends TestCase
{
    private $pay;

    protected function setUp()
    {
        $this->pay = new \App\Payment();
    }

    public function addDataProvider() {
        // Flo2Cash test data
        return array(
            // Test A:
            array(
                array(
                    'FirstName' => 'Joe',
                    'LastName' => 'Smith',
                    'StartDate' => '2019-10-06',
                    'Frequency' => 'oneOff',
                    'Amount' => '10.00',
                    'Reference' => '',
                    'Particular' => 'Test A',
                    'CardNumber' => '4987654321098769',
                    'CardType' => 'VISA',
                    'CardExpiry' => '1220',
                    'CardHolderName' => 'Joe Smith',
                    'CardCSC' => '111'), true),

            // Test B:
            array(
                array(
                    'FirstName' => 'Jane',
                    'LastName' => 'Davis',
                    'StartDate' => '2019-10-06',
                    'Frequency' => 'fortnightly',
                    'Amount' => '10.05',
                    'Reference' => '',
                    'Particular' => 'Test B',
                    'CardNumber' => '4987654321098769',
                    'CardType' => 'VISA',
                    'CardExpiry' => '1220',
                    'CardHolderName' => 'Jane Davis',
                    'CardCSC' => '111'), false)
        );
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testMakePayment($details, $expected)
    {
        $transaction = $this->pay->makePayment($details);
        $result = $transaction['Authorized'];
        $this->assertEquals($result, $expected);
    }
}
