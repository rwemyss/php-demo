### Overview

This PHP demo has been put together to show basic understand of some commonly used concepts and techniques in PHP such as:

* Exposing a REST API
* Utilizing OOP
* SOAP calls to an external API
* Unit testing of code
* Composer and project structure
* GIT version control
* Commandline usage
* General PHP usage

> Note: this is probably not how you would want to handle credit card data in a production environment due to PCI 
compliance, it's just to demonstrate a technical implementation.

### Installation
Check out:
```
$ git clone https://rwemyss@bitbucket.org/rwemyss/php-demo.git
```

Install dependencies:
```
$ composer install
```

### Unit Tests
Run unit test for the Payment Class: 
```
$ ./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/App/PaymentTest.php
```

### Running Locally
Setup a local webserver to test manually via the inbuild webserver in PHP:
```
$ php -S localhost:8000 public/rest/routes.php
```

Send data to the API via cURL:
```
$ curl -d "FirstName=Joe&LastName=Smith&StartDate=2019-10-06&Frequency=oneOff&Amount=10.00&Reference=&Particular=Test A&CardNumber=4987654321098769&CardType=VISA&CardExpiry=1220&CardHolderName=Joe Smith&CardCSC=111" -X POST http://localhost:8000/payment
```
