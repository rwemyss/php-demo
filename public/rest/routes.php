<?php
/**
 * REST API Router
 *
 * @author     Richard Wemyss <rwemyss@gmail.com>
 * @copyright   Richard Wemyss
 */

require_once dirname(__FILE__) . '/../../vendor/autoload.php';

// Setup router framework
$app = new Illuminate\Container\Container;
Illuminate\Support\Facades\Facade::setFacadeApplication($app);

$app['app'] = $app;
$app['env'] = 'production';

with(new Illuminate\Events\EventServiceProvider($app))->register();
with(new Illuminate\Routing\RoutingServiceProvider($app))->register();

/**
 * Routes
 */
$app['router']->post('/payment', function() {
    $payment = new \App\Payment();
    return $payment->makePayment($_POST);
});

$request = Illuminate\Http\Request::createFromGlobals();
$response = $app['router']->dispatch($request);
$response->send();
