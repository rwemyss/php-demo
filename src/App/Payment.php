<?php
/**
 * Payment
 *
 * @author     Richard Wemyss <rwemyss@gmail.com>
 * @copyright   Richard Wemyss
 */

namespace App;

/**
 * This class provides a simple interface to the Flo2Cash payment system suitable for unit
 * testing.
 */
class Payment {

    private $CC_USERNAME;
    private $CC_PASSWORD;
    private $CC_ACCOUNT_ID;
    private $CC_HOST;

    public function __construct()
    {
        // Grab credentials
        /*
        $this->CC_USERNAME = $_ENV["CC_USERNAME"];
        $this->CC_PASSWORD = $_ENV["CC_PASSWORD"];
        $this->CC_ACCOUNT_ID = $_ENV["CC_ACCOUNT_ID"];
        $this->CC_HOST = $_ENV["CC_HOST"];
        */

        // These would normally not be included in the repo, test account details only.
        $this->CC_USERNAME = "51053";
        $this->CC_PASSWORD = "ffd5hjeb";
        $this->CC_ACCOUNT_ID = "11086";
        $this->CC_HOST = "https://sandbox.flo2cash.com/ws/paymentws.asmx?WSDL";
    }

    /**
	 * Allows a single or monthly payment to be made.
	 *
	 * @param array $details
	 * @return array
	 */
    public function makePayment(array $details)
    {
        $payload = array(
            'AccountId' => $this->CC_ACCOUNT_ID,
            'Username' => $this->CC_USERNAME,
            'Password' => $this->CC_PASSWORD,
            'StoreCard' => true,
            'Particular' => '',
            'Email' => '',
            'Amount' => $details['Amount'],
            'Reference' => $details['Reference'],
            'CardNumber' => $details['CardNumber'],
            'CardType' => $details['CardType'],
            'CardExpiry' => $details['CardExpiry'],
            'CardHolderName' => $details['CardHolderName'],
            'CardCSC' => $details['CardCSC']
        );

        try {
            $client = new \SoapClient($this->CC_HOST);
            $result = $client->ProcessPurchase($payload);

            if($result->transactionresult->Status === "SUCCESSFUL") {
                $authorized = true;
                $token = $result->transactionresult->CardToken;
            } else {
                $authorized = false;
                $token = '';
            }

            $response = array(
                "TransID" => $result->transactionresult->TransactionId,
                "Token" => $token,
                "Msg" => $result->transactionresult->Message,
                "ReCo" => $result->transactionresult->AuthCode,
                "Authorized" => $authorized
            );
        } catch (\SoapFault $fault) {
            $response = array(
                "TransID" => '',
                "Token" => '',
                "Msg" => $fault->faultstring,
                "ReCo" => '',
                "Authorized" => false
            );
        }

        return $response;
    }
}
